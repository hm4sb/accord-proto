namespace sb.protocols.orders.models

import org.accordproject.cicero.contract.* from https://models.accordproject.org/cicero/contract.cto
import org.accordproject.cicero.runtime.* from https://models.accordproject.org/cicero/runtime.cto
import org.accordproject.address.PostalAddress from https://models.accordproject.org/address.cto
import org.accordproject.value.QuantitativeValue from https://models.accordproject.org/value.cto
import org.accordproject.money.* from https://models.accordproject.org/money.cto
import org.accordproject.time.* from https://models.accordproject.org/v2.0/time.cto
import sb.protocols.orders.enums.*
import sb.protocols.orders.structs.*

// The order params
asset RawCottonSale extends AccordContract {
  o DateTime saleDate
  o String saleNumber
  o String contractRef optional
  o String buyerName
  o String buyerRegistrationNumber 
  o String buyerAddressStreet
  o String buyerAddressZipCode 
  o String buyerAddressCity 
  o String buyerAddressRegion optional
  o String buyerAddressCountry optional
  o AccordParty buyer 
  o String sellerName 
  o String sellerRegistrationNumber 
  o String sellerAddressStreet
  o String sellerAddressZipCode 
  o String sellerAddressCity 
  o String sellerAddressRegion optional
  o String sellerAddressCountry optional
  o AccordParty seller 
  o CurrencyCode currency
  o Weight weight
  o CottonQuality quality 
  o Boolean allowPartial 
  o Double allowedVariation
  o Double dealAX default = 16.0
  o Double dealA0 default = 15.90
  o Double dealA1 default = 15.65
  o Double dealA2 default = 14.75
  o Double dealB default = 13.20
  o Double dirkAX default = 15.0
  o Double dirkA0 default = 15.50
  o Double dirkA1 default = 15.25
  o Double dirkA2 default = 14.90
  o Double dirkB default = 12.50
  o Double dolyAX default = 15.50
  o Double dolyA0 default = 14.70
  o Double dolyA1 default = 14.45
  o Double dolyA2 default = 13.55
  o Double dolyB default = 12.5
  o Double dunsA1 default = 13.88
  o Double dunsA2 default = 13.00
  o Double dunsB default = 11.70
  o Double lfyA1 default = 13.22
  o Double lfyA2 default = 12.75
  o Double lfyB default = 11.1
  o Duration deliveryPeriod
  o Duration paymentPeriod
  o String workflowId optional 
  o String workflowAxeOrder optional
  o String workflowAxeDelivery optional
  o String workflowAxeInvoice optional
  o String workflowAxePayment optional
}

// the order state
asset RawCottonSaleState extends AccordContractState {
  o DateTime signatureDate optional 
  o SaleStatus status // the status of the contract
  o PODInfo pod optional
  o InvoiceInfo invoice optional // the reference of the invoice 
  o PaymentInfo[] payments optional // the list of payment received for this contract
  o ObligationStatus deliveryObligation default = "NONE"
  o ObligationStatus paymentObligation default = "NONE"
}
