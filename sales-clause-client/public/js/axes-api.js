'use script';

// Call for invoice posting
function invoiceAxe(axe) {
    try{
        let params = [];
        params.push({name: 'BUYER', value: getSBId(axe.buyerId)});
        params.push({name: 'SELLER', value: getSBId(axe.sellerId)});
        params.push({name: 'date', value: axe.date});
        params.push({name: 'currency', value: axe.currency});
        params.push({name: 'value', value: axe.value});
        params.push({name: 'invoiceId', value: axe.invoiceRef});

        postAxe(axe.templateId, params); 
    }
    catch(ex) {
        log(ex);
    }
}

function paymentAxe(axe) {
    try{
        let params = [];
        params.push({name: 'PAYER', value: getSBId(axe.buyerId)});
        params.push({name: 'PAYEE', value: getSBId(axe.sellerId)});
        params.push({name: 'date', value: axe.date});
        params.push({name: 'currency', value: axe.currency});
        params.push({name: 'value', value: axe.value});
        params.push({name: 'invoiceId', value: axe.invoiceRef});
        params.push({name: 'share', value: '100'});

        postAxe(axe.templateId, params); 
    }
    catch(ex) {
        log(ex);
    }
}

// Generic purpose Posting method
function postAxe(templateId, params) {
    let templateDomain = getTemplateDomain(templateId);
    let templateName = getTemplateName(templateId);
    let paramList = formatParameters(params);

    log('Voucher Creation: Begin')
    createVoucher(templateDomain, templateName, paramList)
    .done(creationResult => {
        logObj(creationResult, 'Voucher Creation: Result:');

        if (creationResult.errors) 
        {
            logObj(creationResult.errors, 'Voucher Creation: Error:');
            return; 
        }
        
        postVoucher(creationResult.data.axeVoucherCreate.id)
        .done(postingResult => {
            if (postingResult.errors) 
            {
                logObj(postingResult.errors,  'Voucher Posting: Error:');
                return;
            }
            else {
                logObj(postingResult.data, 'Voucher Posting Result:');
            }            
        })
        .fail(err => logObj(err, 'Axe Posting API Error:'))
    })
    .fail(err => {
        logObj(err, 'Axe Create Voucher API Error:');
    })
    .always(() => log('Done'));
}

// Call to create voucher
function createVoucher(domain, name, params) {    
    let qry = `mutation {
        axeVoucherCreate(
            partitionId: "${$('#sbPartitionId').val()}", 
            domain: "${domain}", 
            name: "${name}",
            inputParameters: [${params}]
        )
        {
            id
        }
      }
      `;

    log(qry);

    return $.post({
        url: $('#sbEndpoint').val(),
        data: JSON.stringify({query: qry}),
        headers: {
            'Content-Type':'application/json'
        }
    });
}

// Post the created voucher
function postVoucher(voucherId) {
    let qry = `
        mutation {
            axeVoucherPost(
                partitionId: "${$('#sbPartitionId').val()}", 
                axeVoucherId: "${voucherId}"
                )
                {
                    postingId
                }
        }
    `;

    log(qry);
    let options = {
        url: $('#sbEndpoint').val(),
        data: JSON.stringify({query: qry}), 
        headers: {
            'Content-Type':'application/json'
        }
    };

    return $.post(options);
}

function formatParameters(paramList) {
    let res = [];
    paramList.forEach(item => {
        res.push(`{name:"${item.name}", value: "${item.value}"}`)
    });
    return res.join(',');
}

function getTemplateDomain(templateId) { return templateId.split('::')[0];}
function getTemplateName(templateId) { return templateId.split('::')[1];}
function getSBId(id) { return 'did:sb:' + id;}