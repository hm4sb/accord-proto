'use script';

function log(msg) {
    logMsg = `[${(new Date()).toUTCString()}]: ${msg}\n` 
    $('#console').append(logMsg); 
}

function logObj(json, label) {
    let js = JSON.stringify(json, null, 4);
    if (label) 
        log(label + '\n' + js);
    else 
        log(js); 
}

function clearLog() { $('#console').empty(); }

