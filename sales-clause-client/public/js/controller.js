'use script';

function setMenu(key) {
  $("#settingsPanel").hide();
  $("#contractPanel").hide();
  $("#deliveryPanel").hide();
  $("#invoicePanel").hide();
  $("#paymentPanel").hide();
  $('consolePanel').hide();

  if (key == "SETTINGS") $("#settingsPanel").show();
  if (key == "CONTRACT") $('#contractPanel').show(); 
  if (key == "DELIVERY") {
    $("#deliveryPanel").show();
    $('#deliveryDate').append((new Date()).toISOString());
  }
  if (key == "INVOICE") $("#invoicePanel").show(); 
  if (key == "PAYMENT") $("#paymentPanel").show();  

  if (key == 'DELIVERY' || key == 'INVOICE' || key == 'PAYMENT') {
    $('#consolePanel').show();
  } else {
    $('#consolePanel').hide();
  }
}

function readContractState() {
  try {
    contractState()
    .done(data => {
      $('#contractParams').empty().append(JSON.stringify(data.contract, null, 4));
      $('#contractState').empty().append(JSON.stringify(data.state, null, 4));
    })
    .fail(err => alert('Error retrieving contract info' + err));
  }
  catch(ex) 
  {}
}


// records a delivery
function recordDelivery() {
  clearLog();
  log('Begin');
  try {
    deliveryClause()
      .done(data => {
        logObj(data, 'Contract Result:');
      })
      .fail(err => logObj(err.responseText, 'Contract Error:')
      .always(() => log('End'))
    );
  }
  catch(ex) {
    log('Exception:' + ex);
  }
}

// Records an invoice
function recordInvoice() {
  clearLog();
  log('Record Invoice')
  try {
    invoiceClause()
      .done(data => {
        logObj(data, 'Contract Result:');
        invoiceAxe(data.axe); 
      })
      .fail(err => logObj(err.responseText, 'Contract Error:'));
  }
  catch(ex) {
    log('Exception:' + ex);
  }
}

// Records a payment
function recordPayment() {
  clearLog();
  try {
    paymentClause()
      .done(data => {
        logObj(data, 'Contract Result:');
        paymentAxe(data.axe); 
      })
      .fail(err => logObj(err.responseText, 'Contract Error:')
      .always(() => 'End'));
  }
  catch(ex) {
    log('Exception:' + ex);
  }
}
