'use script';

function contractState() {
  let req = {};
  req.$class = 'sb.protocols.orders.queries.ContractStateRequest';
  return triggerClause(req);
}


function deliveryClause() {
  let balle = {
    $class: 'sb.protocols.orders.structs.BalleInfo',
    number: '1',
    lot: '1',
    quality: {
      $class:'sb.protocols.orders.structs.CottonQuality', 
      cgrade: $('#deliveryGrade').val(),
      cclass: $('#deliveryClass').val()
    },
    weight: {
      $class: 'sb.protocols.orders.structs.Weight',  
      value: parseFloat($('#deliveryQty').val()), 
      unit: 'KG'  
    }
  };

  let req = {};
  req.$class = 'sb.protocols.orders.queries.DeliveryRequest';
  req.podRef = $('#deliveryRef').val();
  req.podDate = new Date();
  req.balles = [balle];

  return triggerClause(req);
}

function invoiceClause() {
  let req = {};
  req.$class = 'sb.protocols.orders.queries.InvoiceRequest';
  req.invoiceRef = $('#invoiceRef').val();
  req.invoiceDate = new Date();
  req.invoiceAmount = {
    $class: 'sb.protocols.orders.structs.Amount', 
    value: parseFloat($('#invoiceAmount').val())
  };

  logObj(req, 'Before clause');
  return triggerClause(req);
}

function paymentClause() {
  let req = {};
  req.$class = 'sb.protocols.orders.queries.PaymentRequest';
  req.paymentRef = $('#paymentRef').val();
  req.paymentDate = new Date();
  req.paymentAmount = {
    $class: 'sb.protocols.orders.structs.Amount', 
    value: parseFloat($('#paymentAmount').val())
  };

  logObj(req, 'Before clause');
  return triggerClause(req);
}

//--------- Low level triggers
function triggerClause(req, ctx) {
  return $.post({
    url: $('#clauseUrl').val(),
    data: JSON.stringify(req),
    headers: {
      'Authorization': 'Bearer ' + $('#clauseToken').val(),
      'Content-Type': 'application/json'
    }
  });
}